import zipfile
import os


# funkcja sluzaca do rozpakowywania plikow w formacie zip czyli naszego datasetu. Na obecnym etapie prac nad cala
# aplikacja stworzylem taka funkcje do wywolywania ze skryptu glownego
def unzip(pathtofile):
    # save_dir = os.path.join(os.getcwd(), nameoffolder)
    save_dir = os.getcwd()
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    with zipfile.ZipFile(pathtofile, 'r') as zip_ref:
        zip_ref.extractall(save_dir)


if __name__ == "__main__":
    unzip('C:/Users/albi45/Desktop/Audio.zip', 'dataset')
