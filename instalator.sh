#!/bin/bash
clear
echo 'Installing laughgrade please wait...'
sudo apt update
sudo apt install ffmpeg
sudo apt install python3.8 python3.8-venv -y
HOME=$(pwd)
python3.8 -m venv env
source $HOME/env/bin/activate
python -V
pip install --upgrade pip
pip install -r requirements.txt
echo 'Laughgrade installed!'