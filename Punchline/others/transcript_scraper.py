import multiprocessing
import requests
from bs4 import BeautifulSoup
import re

class Scraper :
    """
       Class allows finding jokes in the transcripts on the page https://scrapsfromtheloft.com/stand-up-comedy-scripts/.
    """
    def __init__(self, urls_source_path = None):
        """
            Initializing base class variables
        :param urls_source_path: optional path to file with urls , defaults to None
        :type urls_source_path: string , optional
        """
        self.path = urls_source_path 
        self.urls = [] if self.path is None else self.__load_urls__()
        self.pattern = r'[\[(][a-zA-Z]*[\s]*laugh[a-z]*[\])]' #audience reaction
        self.verbose = 1
        self.sentences_num = 2 #desired length of joke
        self.jokes = []

    def scrap_urls(self):
        """
            Looks for the transcript links on https://scrapsfromtheloft.com/stand-up-comedy-scripts/ site
        """
        page = requests.get('https://scrapsfromtheloft.com/stand-up-comedy-scripts/')
        soup = BeautifulSoup(page.content, 'html.parser')
        results = soup.findAll('a', href=True)

        for i, link in enumerate(results[67 :]) :
            self.urls.append(link['href'])
            if link['href'] == 'https://scrapsfromtheloft.com/2016/11/10/george-carlin-its-bad-for-ya/' :
                break

    def filter_urls(self):
        """
             Filters transcripts without the desired pattern in it
        """
        if self.urls is []:
            self.scrap_urls()
        pool = multiprocessing.Pool(multiprocessing.cpu_count( ))
        self.urls = pool.map(self.__filter__, self.urls)
        self.urls = list(filter(lambda a : a is not None, self.urls))

    def set_filter_pattern(self, pattern):
        """
            Sets desired pattern that transcript should contain
        :param pattern: -
        :type pattern: string
        """
        self.pattern = pattern

    def save_urls(self, path):
        """
            Saves list of filtered urls 
        :param path: path to file   
        :type path: string
        """
        if self.urls:
            f = open(path, 'w')
            f.writelines(self.urls)
            f.close()
        else :
            print("No urls that can be saved")

    def look_for_jokes(self, sentences_num = 2, verbose = 1) :
        """
           Looks for jokes (suitable pattern) on the given pages using self.__finder__ function. In parallel 
        :param sentences_num: , defaults to 2
        :type sentences_num: int, optional
        :param verbose: , defaults to 1
        :type verbose: int, optional
        """
        self.sentences_num = sentences_num
        self.verbose = verbose
        if self.urls is [] :
            self.scrap_urls( )
        pool = multiprocessing.Pool(multiprocessing.cpu_count( ))
        self.jokes = pool.map(self.__finder__, self.urls)
        self.jokes = list(filter(lambda a : a is not None, self.jokes))

    def save_jokes(self,path):
        """
            Saves list of extracted jokes
        :param path: path to file 
        :type path: string 
        """
        if self.jokes:
            jokes = []
            for joke_set in self.jokes:
                jokes.extend(joke_set)

            f = open(path, 'w', encoding='windows-1250')
            for joke in jokes:
                try:
                    f.write(joke + '\n')
                except UnicodeDecodeError:
                    pass
            f.close()
        else:
            print("No jokes that can be saved")

    def __load_urls__(self):
        """
            Loads list of urls from file (optional)
        :return: list of urls 
        :rtype: list 
        """
        f = open(self.path, 'r')
        urls = f.read( ).splitlines( )
        return urls

    def __finder__(self, url):
        """
            Looks for patterns such as [laughter] in the transcript that indicate that a punchline has occurred. 
            N sentences are taken before the requested pattern.
        :param url: page where jokes are to be searched for 
        :type url: string 
        :return: list of jokes 
        :rtype: list 
        """
        try :
            page = requests.get(url)
            soup = BeautifulSoup(page.content, 'html.parser')

            results = soup.findAll('p', attrs={'style' : 'text-align: justify;'}, recursive=True)

            full_transcript = []
            for result in results :
                full_transcript.append(result.findAllNext(text=True))

            full_transcript[0] = list(
                filter(lambda a : a not in ['\n', '\n     (adsbygoogle = window.adsbygoogle || []).push({});\n'],
                       full_transcript[0]))

            max_index = full_transcript[0].index(' AI CONTENT END 1 ')

            text = ' '.join(full_transcript[0][0 :max_index])

            pattern = r''
            for i in range(self.sentences_num):
                pattern += r'[.?!][,“\w\s\d”]+'
            pattern += r'[.?!][\w\s\d”]*(?=[\[(][a-zA-Z]*[\s]*laugh[a-z]*[\])])'

            jokes = re.findall(pattern, text, re.IGNORECASE)
            jokes = list(map(lambda a: a.lstrip('.?!”'), jokes))

            if self.verbose :
                for punchline in jokes :
                    print(punchline)
            return jokes
        except (ValueError, IndexError, requests.exceptions.MissingSchema, UnicodeEncodeError) as e :
            pass

    def __filter__(self, url):
        """
            Optional function for filtering websites using the self.pattern as the criterion
        :param url: url to search for pattern 
        :type url: string 
        :return: url to file with pattern
        :rtype: string 
        """
        try :
            page = requests.get(url)
            soup = BeautifulSoup(page.content, 'html.parser')
            results = soup.findAll('p', attrs={'style' : 'text-align: justify;'}, recursive=True)

            full_transcript = []
            for result in results :
                full_transcript.append(result.findAllNext(text=True))

            full_transcript[0] = list(
                filter(lambda a : a not in ['\n', '\n     (adsbygoogle = window.adsbygoogle || []).push({});\n'],
                       full_transcript[0]))

            max_index = full_transcript[0].index(' AI CONTENT END 1 ')

            text = ' '.join(full_transcript[0][0 :max_index])
            
            if re.search(self.pattern, text, re.IGNORECASE) :
                print(f'[V] Pattern found in {url}')
                return url + '\n'
            else :
                print(f'[X] Pattern not found in {url}')
        except (ValueError, IndexError, requests.exceptions.MissingSchema) as e :
            pass

if __name__ == '__main__' :
    scraper = Scraper(urls_source_path='transcript_urls.txt')
    scraper.look_for_jokes(sentences_num=2)
    scraper.save_jokes('jokes.txt')

