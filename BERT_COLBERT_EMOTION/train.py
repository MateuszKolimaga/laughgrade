import tensorflow as tf
import numpy as np
import os
import read_dataset


def training(train, train_labels, test, test_labels):
    model = tf.keras.Sequential([
        tf.keras.layers.InputLayer(input_shape=(2, 1)),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(32, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(16, activation='relu'),
        tf.keras.layers.Dropout(0.2),
        tf.keras.layers.Dense(1, activation='sigmoid')
    ])

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.MeanSquaredError(),
                  metrics=['accuracy'])

    model.fit(train, train_labels, epochs=25)
    _, test_acc = model.evaluate(test, test_labels, verbose=2)
    print(test_acc)

    save_dir = os.path.join(os.getcwd(), 'zapisany_model')
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    model_path = os.path.join(save_dir, 'model.h5')
    model.save(model_path)

    model_json = model.to_json()
    with open("zapisany_model/model.json", "w") as json_file:
        json_file.write(model_json)


if __name__ == '__main__':
    # train_labels,train,test_labels,test = get_dataset.main(50,10)
    print('uczonko')
    # train = np.array([np.array([0.3, 0.2]), np.array([0.6, 0.5]), np.array([0.7, 0.6]), np.array([0.9, 0.867]),
    #                   np.array([0.6, 0.576]),
    #                   np.array([0.1244, 0.4634]), np.array([0.5646, 0.4646]), np.array([0.5335, 0.598]),
    #                   np.array([0.855, 0.667])])
    # train_labels = np.array([0.5, 0.4, 0.5, 0.3, 0.9, 0.3, 0.1, 0.4, 0.6])
    # test = np.array([np.array([0.3, 0.2]), np.array([0.6, 0.5]), np.array([0.7, 0.6])])
    # test_labels = np.array([0.7, 0.1, 0.9])
    train,train_labels,test,test_labels = read_dataset.read()
    training(train, train_labels, test, test_labels)
