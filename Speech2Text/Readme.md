# OPIS
Moduł Speech2Text odpowiada za transkrypcję nagrania z pliku .wav
#Działanie
Moduł po wczytaniu pliku .wav , sprawdza jego długość , jeśli plik jest długi
to za pomocą funkcji split_on_silence z modułu pydub
```
 chunks = split_on_silence(sound, min_silence_len=300, silence_thresh=sound.dBFS - 16.5)
```
skrypt dzieli nagranie .wav na krótsze nagrania ,
i przechowuje je w folderze sr-chunks. <br /><br />Następnie skrypt za pomocą API od Google'a nagranie wysyłane jest 
przez internet do przetwarzania <br />
```
text = r.recognize_google(audio_listened, language="en-US")
```
i po chwili otrzymujemy jego transkrypcję.<br /><br />
Na koniec za pomocą modułu punctuator 
```
p = Punctuator(model_path)
res = p.punctuate(res)
```
dodajemy do tekstu interpunkcję i zwracamy całą wartość   
#Testowanie
Działanie modułu zostało przetestowane na 447 próbkach o długości od 3 do 15 sekund <br />
dla każdej próbki ze zbioru testowego , porównuje jej transkrypcję z otrzymanym wynikiem z 
modułu Speech2Text za pomocą SequenceMatcher z modułu difflib.<br />
```
SequenceMatcher(None, data[1], response.upper())
```
następnie obliczana jest średnia wartość zgodności tekstów która wynosi ~95%