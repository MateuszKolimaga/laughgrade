# ColBert
ColBert jest rozwiązaniem opartym na technologii BERT. Odpowiada on za określenie śmieszności danego zdania otrzymanego
z transkrypcji pliku .wav. Wyniki predykcji są z przedziału od 0 do 1.
Do nauczenia modelu sieci i przetestowania go został wykorzystany gotowy dataset wielkości 200000 przykładowych,
krótkich tekstów bedących lub nie będących żartem. 160000 było wykorzystanych do nauczenia modelu sieci, a na
pozostałych 40000 tekstów zostało sprawdzone accuracy sieci które wynosi 99.3% dokładności w określaniu śmieszności
zdania. 

- "testowanie.py" - skrypt odpowiada za przetestowanie gotowego modelu sieci, możli do pobrania z [Dysku](https://drive.google.com/file/d/1_H_Y-ssQNJaXc4NxG1fUHSdaFJeyHUNk/view?usp=sharing)
- "uczenie.py" - skrypt umożliwia stworzenie nowego modelu sieci np. wykorzystując gotowy dataset z [Dysku](https://drive.google.com/file/d/1yKkVXpRzQNF_zmWmViN63Ow-2rALLkeI/view?usp=sharing)
- "test_accuracy.py" - skrypt umożliwia sprawdzenie accuracy wykorzystywanego modelu na podstawie pliku .csv uzyskanego podczas odpalania skryptu "testowanie.py"
# Uczenie sieci 

 W celu nauczenia sieci w skrypcie "uczenie.py" pobieramy dataset
 z [Dysku](https://drive.google.com/file/d/1yKkVXpRzQNF_zmWmViN63Ow-2rALLkeI/view?usp=sharing) i zapisujemy go w
 folderze ColBERT. Następnie w skrypcie ustawiamy wielkość datasetu do uczenia sieci oraz podajemy ścieżkę zapisu
 modelu nauczonej sieci.

```
saved_model_path = 'test300'
```
```
training_sample_count = 300  # 4000 # 8000
training_epochs = 1  # 3
```
# Testowanie sieci

 Gdy sieć jest już nauczona lub został pobrany z dysku wcześniej nauczony
 [Model Sieci](https://drive.google.com/file/d/1_H_Y-ssQNJaXc4NxG1fUHSdaFJeyHUNk/view?usp=sharing) możemy skorzystać ze
 skryptu "testowanie.py", aby przetestować nauczony model.
  
  Konieczne jest podanie prawidłowej ścieżki do zapisanego modelu sieci oraz pliku .csv ze zdaniami do predykcji!!!
```
if __name__ == "__main__":
    predict('colbert_model', "test30k.csv")
```
Można również zmienić ścieżkę oraz nazwę pliku z wynikami predykcji.
```
    saved_test_path = "test_wynik30k.csv"
```
# Sprawdzanie accuracy ColBERTA

 Pliki: 
- "test30k.csv"
- "test10k.csv"
- "test_wynik30k.csv"
- "test_wynik10k.csv"

Można pobrać z naszego [Dysku](https://drive.google.com/drive/folders/1E8dz2UUmKHHewfzh2fAlZub0Ai7Rykcg?usp=sharing)
aby sprawdzić dokładność sieci samemu.

 W celu sprawdzenia dokładności ColBERTA wykorzystujemy skrypt "test_accuracy.py",
 który wczytuje plik "test_wynik30k.csv"
 ``` df_test = pd.read_csv('test_wynik30k.csv')```
zawierający wyniki z predykcji naszej sieci na 30000 zdaniach.
 Skrypt podlicza poprawnie oraz źle określone śmieszności zdań i na tej podstawie wylicza dokładność
 sieci.

 Uzyskane accuracy na:
- 30000 zdaniach: 99.3%
- 10000 zdaniach: 98.4%