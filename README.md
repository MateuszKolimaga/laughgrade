# LaughGrade

## Opis projektu
_Na podstawie różnego rodzaju standup’ów i wystąpień TED należy opracować predyktor śmiania się widowni._

## Konstrukcja projektu
Projekt składa się z 4 głównych skryptów, które łączą się ze sobą i pomagają predykować śmianie się widowni. Są to:
1. Skrypt rozpoznający mowę ludzką (**Speech 2 Text - S2T**)
   
2. Skrypt rozpoznający emocje mówcy (**Emotion Analisys - EA**)
   
3. Skrypt oceniający śmieszność zdania (**ColBert**)
   
4. Skrypt wykrywający punchline (**Small Bert**)


_**Speech 2 Text** - za pomocą Web Speech API od Google'a rozpoznaje mowę ludzką._
  
_**Emotion Analisys** - dzieli plik audio na 3 sekundowe odcinki, a następnie predykuje na podstawie wytrenowanego wcześniej modelu Sekwencyjnej Sieci Neuronowej emocje z jaką wypowiadany był fragment zdania. Ostatecznie kategoryzuje jak pozytywna byłą ta emocja ( w skali od 0.1 do 0.9 )_

_**ColBERT** - na podstawie modelu przetrenowanego na datasecie zawierających 160000 żartów/nie żartów, predykuje czy dane zdanie jest śmieszne, czy nie, a następnie tworzyu plik .csv ze zdaniami i prawdopodobieństwem ich śmieszności_

_**Small BERT** - kategoryzuje zdania, badając jego kontekst i predykując czy następuje w nim, pod koniec rozwiązanie - punchline, tym samym oceniając prawdopodobieństwo śmiania się widowni._

## Skrócony opis działania
Na wejściu programu podawana jest ścieżka do pliku audio (format .wav). 
Następnie odpalany jest skrypt rozpoznający mowę (S2T) oraz skrypt Rozpoznający Emocje (EA).   
Na wyjściu skryptu S2T, otrzymujemy tekst - sformatowany, z dodaną interpunkcją, a na wyjściu skryptu EA - wartości z przedziału 0.1-0.9 w zależności od tego jak pozytywna (szczęśliwa, spokojna) jest osoba, wymawiająca dane zdanie.   
Wyjście skryptu S2T podawane jest następnie na wejście sieci ColBERT - wykrywającej śmieszność danego zdania.  
Następnie zdania z transkryptu przekazywane są na sieć wykrywającą punchline'y.
Na sam koniec tworzone są pliki .csv i .txt z przetwarzanymi zdaniami, ich śmiesznością oraz kategorią punchline/nie punchline, oraz emocjami wykrytymi przez skrypt EA.

## Wymagania
Aby zapewnić poprawne działanie projektu należy odpalić go na systemie operacyjnym: **Ubuntu 18.04**. Należy mieć także zainstalowaną wersję pythona: **Python 3.8**.

## Sposób uruchomienia
Po spełnieniu wymagań, aby uruchomić projekt należy pobrać repozytorium,oraz odpalić skrypt instalator.sh, który pobiera paczkę ffmpeg, potrzebną do konwertowania plików do formatu .wav, oraz instaluje potrzebne moduły python'owe z pliku requirements.txt:
``` bash
$ git clone https://gitlab.com/GabrielQuik/laughgrade.git
$ cd laughgrade
$ sudo chmod +x instalator.sh
$ ./instalator.sh
```

Następnie po zakończeniu procesu instalacji przez skrypt instalator.sh, należy aktywować środowisko wirtualne:

```bash
$ source env/bin/activate
```

Kolejny krok to pobranie oraz umieszczenie w folderze projektu pliku zip z przetrenowanymi modelami sieci z naszego dysku google:

[saved_models.zip](https://drive.google.com/drive/u/0/folders/1ApPsT10MS6GT6az13JwYUA5Hh7Gk9evn)

Po pobraniu i umieszczeniu pliku saved_models.zip, struktura projektu powinna wyglądać w taki sposób:

```
├── ColBERT
│   ├── Readme.md
│   ├── test_accuracy.py
│   ├── testowanie.py
│   └── uczenie.py
├── DatasetMockup
│   └── english1.wav
├── EmotionAnalysis
│   ├── cutter.py
│   ├── predict.py
│   ├── run.py
│   └── training.py
├── Punchline
│   ├── BERT
│   │   ├── predict_SR.py
│   │   ├── predict_standup.py
│   │   ├── predict_ted.py
│   │   ├── README.md
│   │   ├── reqs.txt
│   │   └── train.py
│   └── others
│       ├── BERT+SR_example_results.txt
│       ├── jokes.txt
│       ├── transcript_scraper.py
│       └── transcript_urls.txt
├── README.md
├── run_project.py
├── saved_models.zip
├── Speech2Text
│   └── SpeechToText.py
└── unzip.py
```

Ostatecznie możemy uruchomić projekt z wiersza poleceń, wpisując jako argument ścieżkę do pliku .wav, którego śmieszność chcemy ocenić, np.:
```
$(env) python run_project.py ./DatasetMockup/english1.wav
```
Po uruchomieniu projektu, modele zostaną automatycznie rozpakowane i załadowane do odpowiednich skryptów. Następnie rozpocznie się analiza emocji i proces transkrypcji, z których wyników będzie korzystał skrypt do wykrywania śmieszności oraz punchline'ów.
## Wyniki
Osiągnięte przez nas w projekcie wyniki, najlepiej przedstawiają atrybuty pomiarowe poszczególnych modeli:
```
Accuracy = (TP + TN) / (TP + TN + FP + FN)

Precision = (TP) / (TP + FP)

Recall = (TP) / (TP + FN)

Fscore = 2 * (Precision * Recall) / (Precision + Recall)
```
gdzie:

_TP_ - True Positive
_TN_ - True Negative
_FP_- False Positive
_FN_- False Negative

### Small Bert - Punchline Recognition

Model przetestowany na zbiorze testowym zawierającym ponad 1000 zestawów dwóch zdań, połowa kontekst + punchline, druga połowa kontekst + nie punchline.
<img src="/assets/bert1.png" alt="" style="width:70%"/>


_Accuracy = 0.7375 
Precision = 0.7155
Recall = 0.7833 
Fscore = 0.7479_

### ColBERT - Joke Recognition

Wykrywanie śmieszności zostało przetestowane na zbiorze zawierającym 40000 krótkich tekstów, przygotowanym przez twórców modelu.
<img src="/assets/colbert1.png" alt="" style="width:70%"/>

_Accuracy = 0.6785
Precision = 0.8000
Recall = 0.7619
Fscore = 0.7804_

### Speech To Text
Działanie modułu zostało przetestowane na 447 próbkach o długości od 3 do 15 sekund, dla każdej próbkiza pomocą klasy SequenceMatcher z modułu **difflib** obliczana jest zgodność translacji tekstu z tekstem otrzymanym dzięki Speech2Text. Średnia skuteczność wynosiła około 95%.

_Accuracy = 0.95_

### Emotion Analysis
Rozpoznawanie emocji zostało przetestowane na 3320 plikach audio, z części testowej datasetu. Uzyskana dokładność wynosiła około 74%.

_Accuracy = 0.7439_
## Założenia projektowe

- [x] Rozpoznawanie i rozumienie angielskiej mowy ludzkiej z próbki nagrania standup'u.

- [x] Przetwarzanie angielskiej mowy ludzkiej z nagrania standup'u na tekst(język pisany).

- [x] Rozpoznawanie humoru/śmieszności wypowiedzi z ponad 65% precyzją.

- [x] Określenie miejsca punchline’ów z ponad 62% precyzją.

