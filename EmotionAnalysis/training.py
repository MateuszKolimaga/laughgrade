""""
    Creating and training models for recognizing emotion in speech and saving them into saved_models directory
"""
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten, Dropout, Activation
from keras.layers import Conv1D, MaxPooling1D
from sklearn.utils import shuffle
from keras.utils import np_utils
from sklearn.preprocessing import LabelEncoder
import os
import pandas as pd

# Wczytanie plików z datasetu
mylist = os.listdir('Audio/')

# Ustawienie etykiet
feeling_list = []
for item in mylist:
    if item[6:-16] == '02' and int(item[18:-4]) % 2 == 0:
        feeling_list.append('female_calm')
    elif item[6:-16] == '02' and int(item[18:-4]) % 2 == 1:
        feeling_list.append('male_calm')
    elif item[6:-16] == '03' and int(item[18:-4]) % 2 == 0:
        feeling_list.append('female_happy')
    elif item[6:-16] == '03' and int(item[18:-4]) % 2 == 1:
        feeling_list.append('male_happy')
    elif item[6:-16] == '04' and int(item[18:-4]) % 2 == 0:
        feeling_list.append('female_sad')
    elif item[6:-16] == '04' and int(item[18:-4]) % 2 == 1:
        feeling_list.append('male_sad')
    elif item[6:-16] == '05' and int(item[18:-4]) % 2 == 0:
        feeling_list.append('female_angry')
    elif item[6:-16] == '05' and int(item[18:-4]) % 2 == 1:
        feeling_list.append('male_angry')
    elif item[6:-16] == '06' and int(item[18:-4]) % 2 == 0:
        feeling_list.append('female_fearful')
    elif item[6:-16] == '06' and int(item[18:-4]) % 2 == 1:
        feeling_list.append('male_fearful')
    elif item[:1] == 'a':
        feeling_list.append('male_angry')
    elif item[:1] == 'f':
        feeling_list.append('male_fearful')
    elif item[:1] == 'h':
        feeling_list.append('male_happy')
    elif item[:1] == 'c':
        feeling_list.append('male_calm')
    elif item[:2] == 'sa':
        feeling_list.append('male_sad')
    elif item[:2] == 'af':
        feeling_list.append('female_angry')
    elif item[:2] == 'ff':
        feeling_list.append('female_fearful')
    elif item[:2] == 'hf':
        feeling_list.append('female_happy')
    elif item[:2] == 'cf':
        feeling_list.append('female_calm')
    elif item[:3] == 'saf':
        feeling_list.append('female_sad')

labels = pd.DataFrame(feeling_list)

# Pozyskanie parametrów plików dźwiękowych przy użycu biblioteki librosa
df = pd.DataFrame(columns=['feature'])
bookmark = 0
for index, y in enumerate(mylist):
    X, sample_rate = librosa.load('Audio/' + y, res_type='kaiser_fast', duration=2.5, sr=22050 * 2, offset=0.5)
    sample_rate = np.array(sample_rate)
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=13), axis=0)
    feature = mfccs
    df.loc[bookmark] = [feature]
    bookmark = bookmark + 1

df3 = pd.DataFrame(df['feature'].values.tolist())
newdf = pd.concat([df3, labels], axis=1)
rnewdf = shuffle(newdf)
rnewdf = rnewdf.fillna(0)

# Podział danych na zestawy treningowy, walidacyjny i testowy
train_size = int(0.8 * (len(rnewdf)))
test_size = int(0.1 * (len(rnewdf)))

train = rnewdf[:train_size]
test = rnewdf[train_size:train_size + test_size]
val = rnewdf[train_size + test_size:-1]

trainfeatures = train.iloc[:, :-1]
trainlabel = train.iloc[:, -1:]
valfeatures = val.iloc[:, :-1]
vallabel = val.iloc[:, -1:]
testfeatures = test.iloc[:, :-1]
testlabel = test.iloc[:, -1:]
X_train = np.array(trainfeatures)
y_train = np.array(trainlabel)
X_val = np.array(valfeatures)
y_val = np.array(vallabel)
X_test = np.array(testfeatures)
y_test = np.array(testlabel)

lb = LabelEncoder()

y_train = np_utils.to_categorical(lb.fit_transform(y_train))
y_val = np_utils.to_categorical(lb.fit_transform(y_val))
y_test = np_utils.to_categorical(lb.fit_transform(y_test))

# Zmiana wymiarów do modelu CNN
x_traincnn = np.expand_dims(X_train, axis=2)
x_valcnn = np.expand_dims(X_val, axis=2)
x_testcnn = np.expand_dims(X_test, axis=2)


model = Sequential()
model.add(Conv1D(256, 5, padding='same', input_shape=(216, 1)))
model.add(Activation('relu'))
model.add(Conv1D(128, 5, padding='same'))
model.add(Activation('relu'))
model.add(Dropout(0.1))
model.add(MaxPooling1D(pool_size=8))
model.add(Conv1D(128, 5, padding='same'))
model.add(Activation('relu'))
model.add(Conv1D(128, 5, padding='same'))
model.add(Activation('relu'))
model.add(Conv1D(64, 5, padding='same'))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(Conv1D(64, 5, padding='same'))
model.add(Activation('relu'))
model.add(Flatten())
model.add(Dense(10))
model.add(Activation('softmax'))
opt = keras.optimizers.RMSprop(lr=0.00001, decay=1e-6)
model.summary()
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

cnnhistory = model.fit(x_traincnn, y_train, batch_size=16, epochs=700, validation_data=(x_valcnn, y_val), shuffle=True)
test_loss, test_acc = model.evaluate(x_testcnn, y_test, verbose=2)
print('\nTest accuracy:', test_acc)

# Wykres pokazujący postępy uczenia sieci
plt.plot(cnnhistory.history['loss'])
plt.plot(cnnhistory.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

model_name = 'Emotion_Voice_Detection_Model.h5'
save_dir = os.path.join(os.getcwd(), 'saved_models')

# Zapisywanie modelu i wag
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
model_path = os.path.join(save_dir, model_name)
model.save(model_path)
print('Saved trained model at %s ' % model_path)

# Zapisywanie do pliku json
model_json = model.to_json()
os.chdir(save_dir)
with open("model.json", "w") as json_file:
    json_file.write(model_json)
